import numpy as np
from math import pi
from kernels import sigmoid, relu

class FeedForwardBinaryNet(object):
  '''
  Two layer neural network using relu activations and sigmoid binary output
  '''
  def __init__(self, width):
    '''
      width - Number of neurons in the hidden layer
    '''
    self.width = width
    self.loss_history = []
    self.learning_rate = 0.1
    self.epoch_callback = None
    self.epoch_callback_interval = 10

  def forward(self, X):
    '''
      Make a single forward pass over the data
      Compute activations
    '''
    z1 = np.add(np.dot(self.w1, X), self.b1)
    a1 = relu(z1)
    z2 = np.add(np.dot(self.w2, a1), self.b2)
    a2 = sigmoid(z2)
    return z1, z2, a1, a2

  def backward(self, X, Y, z1, a1, z2, a2):
    # calculate derivatives
    # https://www.youtube.com/watch?v=P7_jFxTtJEo&list=PLkDaE6sCZn6Ec-XTbcX1uRg2_u4xOEky0&index=32
    # https://www.youtube.com/watch?v=7bLEWDZng_M&list=PLkDaE6sCZn6Ec-XTbcX1uRg2_u4xOEky0&index=33
    avgr = 1 / X.shape[1]
    dz2 = a2 - Y
    dw2 = avgr * np.dot(dz2, a1.T)
    db2 = avgr * np.sum(dz2).ravel()
    dz1 = np.copy(z1)
    dz1[dz1 > 0] = 1
    dz1[dz1 < 0] = 0
    dz1 = np.dot(self.w2.T, dz2) * dz1
    dw1 = avgr * np.dot(dz1, X.T)
    db1 = avgr * np.sum(dz1, axis=1, keepdims=True)
    return dw1, db1, dw2, db2

  def learn(self, train_x, train_y, epochs):
    '''
      train_x - Training examples as columns
      train_y - Training labels
      epochs - Number of epochs to train for
    '''
    # initialize parameters
    n_x = train_x.shape[0]
    avgr = 1 / train_x.shape[1]
    self.b1 = np.zeros((self.width, 1))
    self.b2 = 0
    self.w1 = np.random.random((self.width, n_x))
    self.w2 = np.random.random((1, self.width))

    for i in range(epochs):
      # make a forward pass and calculate the loss
      z1, z2, a1, a2 = self.forward(train_x)
      dw1, db1, dw2, db2 = self.backward(train_x, train_y, z1, a1, z2, a2)
      loss = np.multiply(avgr, np.sum((train_y - a2) ** 2).ravel())

      # update parameters
      self.w2 -= dw2 * self.learning_rate
      self.b2 -= db2 * self.learning_rate
      self.w1 -= dw1 * self.learning_rate
      self.b1 -= db1 * self.learning_rate

      if self.epoch_callback:
        if i == 0 or (i+1) % self.epoch_callback_interval == 0:
          self.epoch_callback(i+1, loss)

  def predict(self, x):
    z1, z2, a1, a2 = self.forward(x)
    return np.round(a2)

class FeedForwardMulticlassNet(object):
  def __init__(self, width):
    '''
      width - Number of neurons in the hidden layer
    '''
    self.width = width
    self.loss_history = []
    self.learning_rate = 0.1
    self.epoch_callback = None
    self.epoch_callback_interval = 10

  def forward(self, X):
    '''
      Make a single forward pass over the data
      Compute activations
    '''
    z1 = np.add(np.dot(self.w1, X), self.b1)
    a1 = relu(z1)
    z2 = np.add(np.dot(self.w2, a1), self.b2)
    a2 = sigmoid(z2)
    return z1, z2, a1, a2

class FeedForwardDeepNet(object):
  pass

class ConvolutionalDeepNet(object):
  pass

class RecurrentNet(object):
  pass

class LongShortTermMemoryNet(object):
  pass

if __name__ == '__main__':
  import csv
  import pandas

  def create_epoch_callback(x, y):
    def cb(epoch, loss):
      # Test the network
      # Predict the sex of bears in the test set
      prediction = ffn.predict(x.T).ravel()

      # Eval results
      correct = len(np.where(np.equal(prediction, y))[0])
      total = y.shape[0]
      accuracy = correct / total
      print('[{}]: Training Loss {} | Test Accuracy {}%'.format(epoch, np.round(loss[0], 8), np.round(accuracy * 100, 2)))
    return cb

  # Load the bears dataset
  print('-------------- Bears! -----------------')
  with open('./datasets/bears.csv') as f:
    data = csv.DictReader(f)

    # Create normalized feature vectors
    features = ['HEADLEN', 'HEADWTH', 'NECK', 'LENGTH', 'CHEST', 'WEIGHT']
    X = np.array([[row[i] for i in features] for row in data], dtype='float')
    X = X/X.max(axis=0)

    # Create target labels
    f.seek(0)
    data = csv.DictReader(f)
    Y = np.array([row['SEX'] for row in data], dtype='float')
    Y[Y == 1] = 0
    Y[Y == 2] = 1

    # Split the data into test and train sets
    x_train = X[:30]
    y_train = Y[:30]
    x_test = X[30:]
    y_test = Y[30:]

  # Train the network
  # Note: 
  #   Acheiving about 83% accuracy with: 
  #   random seed = 0 
  #   width  = 8 
  #   learning_rate = 0.1 
  #   epochs = 2000
  width = 10
  epochs = 2000
  np.random.seed(0)
  ffn = FeedForwardBinaryNet(width)
  ffn.learning_rate = 0.1
  ffn.epoch_callback = create_epoch_callback(x_test, y_test)
  ffn.epoch_callback_interval = 100
  ffn.learn(x_train.T, y_train, epochs)

  # Load the mushrooms dataset and one hot encode it
  print('-------------- Mushrooms! -----------------')
  np.random.seed(0)
  df = pandas.read_csv('./datasets/mushrooms.csv')
  df = pandas.get_dummies(df)
  df = df.drop(columns=['class_p'], axis=1)
  Y = df.ix[:,0].values
  df = df.drop(columns=['class_e'], axis=1)
  X = df.values
  n_t = 7000
  x_train = X[:n_t]
  y_train = Y[:n_t]
  x_test = X[n_t:]
  y_test = Y[n_t:]
  print(y_test.shape)
  
  # Train the network
  # Note: 
  # Acheiving about 99% accuracy with:
  #   train-test split 7000/1124
  #   random seed = 0
  #   width = 256
  #   learning_rate = 0.05
  #   epochs = 2000
  width = 256
  epochs = 2000
  ffn = FeedForwardBinaryNet(width)
  ffn.learning_rate = 0.05  
  ffn.epoch_callback = create_epoch_callback(x_test, y_test)
  ffn.epoch_callback_interval = 100
  ffn.learn(x_train.T, y_train, epochs)
